import Layout from '../components/Layout'
import '../styles/globals.css'
import  Login  from './login'
import { useRouter } from 'next/router'

function MyApp({ Component, pageProps }) {
  const router = useRouter()
  console.log('calling _app',router.pathname)
  return router.pathname == '/' ?  <Layout><Component {...pageProps} /></Layout> :  <Login />
 // return <Login />
  //return <Layout><Component {...pageProps} /></Layout>
}

export default MyApp
