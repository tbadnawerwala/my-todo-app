import Head from 'next/head'
import styles from '../styles/Card.module.css'
import axios from 'axios'
import { useEffect,useState } from 'react'
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";

const useStyles = makeStyles({
  root: {
    minWidth: 275,
    maxWidth: 275,
    height: 280,
    display: "inline-block",
    margin: "2%",
  },

  bullet: {
    display: "inline-block",
    margin: "0 2px",
    transform: "scale(0.8)",
  },
  title: {
    fontSize: 14,
  },
  titleData: {
    fontSize: "1rem",
  },
  pos: {
    marginBottom: 12,
  },
});

export default function Home() {
  const classes = useStyles();
  const bull = <span className={classes.bullet}>•</span>;
  const [values, setValues] = useState([]);
  useEffect( async ()=>{
    const { data } = await axios.get(`https://jsonplaceholder.typicode.com/posts`);
    setValues(data);
  },[])

  return values.map((item) => {
  return (
    <Card className={classes.root}>
    <CardContent>
      <Typography
        className={classes.title}
        color="textSecondary"
        gutterBottom
      >
        Title:
      </Typography>
      <Typography className={classes.titleData} variant="h5" component="h2">
        {item.title}
        <br />
      </Typography>
      <br />
      <Typography>
        Description: <br />
      </Typography>
      <Typography variant="body2" component="p">
        {item.body}
        <br />
      </Typography>
    </CardContent>
    <CardActions></CardActions>
  </Card>

  )
});
}
