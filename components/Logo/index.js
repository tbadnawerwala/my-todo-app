
import React from "react";
import styles from '../../styles/Logo.module.css'

export function Logo(props) {
  return (
    <div className={styles.logoWrapper}>
      <div className={styles.logoImg}>
        <img src='to-do.png' alt="Greenland logo" />
      </div>
      <h2 className={styles.logoText}>Todo</h2>
    </div>
  );
}