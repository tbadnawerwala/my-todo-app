import { useMediaQuery } from "react-responsive";
import { Logo } from "../Logo/index";
import { LogoutButton } from "./logoutButton";
import { NavLinks } from "./navLinks";
import { MobileNavLinks } from "./mobileNavLinks";
import styles from '../../styles/Navbar.module.css'

export default function Navbar() {
  const isMobile = useMediaQuery({ maxWidth: '760px' });

  return (
    <div className={styles.navBarContainer}>
      <div className={styles.leftSection}>
        <Logo />
      </div>
      <div className={styles.middleSection}>
          {!isMobile && <NavLinks />}
      </div>
      <div className={styles.rightSection}>
        {!isMobile && <LogoutButton />}
        {isMobile && <MobileNavLinks />}
      </div>
      </div>
  );
}