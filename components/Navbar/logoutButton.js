import styles from '../../styles/LogoutButton.module.css';
import { useRouter } from "next/router";

export function LogoutButton() {
  const router = useRouter();
  const handleLogout = () =>{
    router.push('/login')
  }
  return (
    <div className={styles.container}>
      <button className={styles.logoutButton} onClick={handleLogout}>Logout</button>
    </div>
  );
}