import styles from '../../styles/NavLinks.module.css';

export function NavLinks() {
  return (
    <div className={styles.navLinkContainer}>
      <ul className={styles.linksWrapper}>
        <li className={styles.linkItem}> 
          <a className={styles.link} href="#">About us</a>
        </li>
        <li className={styles.linkItem}> 
          <a className={styles.link} href="#">How it works</a>
        </li>
        <li className={styles.linkItem}>
          <a className={styles.link} href="#">Explore</a>
        </li>
        <li className={styles.linkItem}>
          <a className={styles.link} href="#">Impact</a>
        </li>
      </ul>
    </div>
  );
}